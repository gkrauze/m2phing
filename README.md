# Magento2 phing targets

## Install
At your project dir
1. Get Magento2 phing targets:
```
git clone git@bitbucket.org:gkrauze/m2phing.git
```
2. Install build.xml in your project 
```
cd m2phing
./install.sh
```
3. Setup build.properties and app/etc/env.php
4. [Optional] Install additional dependencies

* for visualization instal dot
```
sudo apt install graphviz
```
* for a3.download install extension amazon awscli
```
sudo apt install awscli
```
If you don't have amazon extension just use `-DnoA3` argument.

5. Enjoy!

## Usage

![build.xml.png](build.xml.png "Targets visualized")
Arrows represent dependencies of the target


Default target:
-------------------------------------------------------------------------------
 cache.flush 

Main targets:
-------------------------------------------------------------------------------
| target name             | Description
|---                      | ---
 _visualize.file          | Generates file per xml build
 a3.download              | Download and gunzip sql backup from Amazon3
 admin-user               | Create hardcoded admin user store_@dmin
 cache.clear              | Cache clear
 cache.flush              | cache flush
 composer.install         | Install PHP dependencies
 db.create                | Create Db if doesn't exist and gives proper privileges
 db.drop                  | Drop Db
 db.load                  | Load SQL file to Db given in env.php
 db.recreate              | Deletes and creates Db
 db.update.clean          | Run Database cleanup for development
 db.user.create           | Create magento user
 gulp.check-fix-with-npm  | [Start] Check if gulp is lower than 4.x and nodejs is greater than 12.x
 gulp.deploy              | Gulp after deploy static content
 gulp.start               | [Start] First time gulp setup
 gulp.version             Get version of gulp at snowdog vendor dir
 m2.build                 | Standard build after picking the branch to work with
 m2.file-permissions      | [Start] Set file permissions, requires sudo if project has for example cache or log files already included.
 m2.front.deploy          | Daily routine to run after implementing frontend changes. You can provide m2theme and m2languages, i.e. -Dm2theme=magento/blank -Dm2language='nb_NO en_US'. Default - all themes in nb_NO and en_US
 m2.front.install         | Run for full download backup, creating database, loading data to frontend deployment
 m2.front.reinstall       | If you want to drop your database and vendors and do all installation again
 m2.front.start           | Run just after cloning the repository and creating database
 m2.reindexall            | Reindex magento
 m2.upgrade               | Run setup:upgrade
 m2.version               | Get magento version in dot format
 nginx.enable             | Creates nginx site configuration, symbolic link in sites-anabled, reload nginx
 npm.install              | Install node dependencies
 npm.node.version         | Get node version
 php.cs                   | PHP CS report
 php.unit                 | Run tests
 php.version              | Get current PHP version number
 php.version.change       | Change version of PHP at ubuntu
 redis.clear              | Redis flush
 showconf                 | Print some configuration properties
 validate-db-properties   | Ensure all required db properties are defined
 visualize                | Generates graphical representation of build files

Subtargets:
-------------------------------------------------------------------------------

| target name             | Description
|---                      | ---
| _init                   |  
| _visualize.init         |  
| m2.build._pre           | Override it in build.xml for project specific actions before m2.build
| m2.upgrade._pre         | Override it in build.xml for project specific actions before m2.upgrade
| ngrok.disable
| ngrok.enable
| static.clear


# Examples
1. 
```
phing m2.front.install
```
2.
```
phing m2.front.install -DnoA3
```
3.
```
phing m2.front.deploy
```
4.
```
phing showconf -Dareas=db
```
