<?php

use Phing\Task;

class GenConfigTask extends ShowConfigTask
{

    private $outputpath = './magento.properties';

    /**
     * Setter for output_path
     *
     * @param string $str
     * @return void
     */
    public function setOutputPath(string $str)
    {
        $this->outputpath=$str;
    }
    
    public function main()
    {
        file_put_contents($this->outputpath, implode("\n", $this->array2path($this->config)));
    }
}
