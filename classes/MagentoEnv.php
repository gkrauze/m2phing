<?php

use Phing\types\DataType;

class MagentoEnv extends DateType
{

    private $url;
    private $username;
    private $password;
    private $persistent = false;

    /**
     * Sets the URL part: mysql://localhost/mydatabase
     *
     * @param string $url
     * @return void
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Sets username to use in connection.
     *
     * @param string $username
     * @return void
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Sets password to use in connection.
     *
     * @param string $password
     * @return void
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Set whether to use persistent connection.
     * @param boolean $persist
     */
    public function setPersistent($persist)
    {
        $this->persistent = (boolean) $persist;
    }

    public function getUrl(Project $p)
    {
        if ($this->isReference()) {
            return $this->getRef($p)->getUrl($p);
        }
        return $this->url;
    }

    public function getUsername(Project $p)
    {
        if ($this->isReference()) {
            return $this->getRef($p)->getUsername($p);
        }
        return $this->username;
    }

    public function getPassword(Project $p)
    {
        if ($this->isReference()) {
            return $this->getRef($p)->getPassword($p);
        }
        return $this->password;
    }

    public function getPersistent(Project $p)
    {
        if ($this->isReference()) {
            return $this->getRef($p)->getPersistent($p);
        }
        return $this->persistent;
    }

    /**
     * Gets a combined hash/array for DSN as used by PEAR.
     * @return array
     */
    public function getPEARDSN(Project $p)
    {
        if ($this->isReference()) {
            return $this->getRef($p)->getPEARDSN($p);
        }

        include_once 'DB.php';
        $dsninfo = DB::parseDSN($this->url);
        $dsninfo['username'] = $this->username;
        $dsninfo['password'] = $this->password;
        $dsninfo['persistent'] = $this->persistent;

        return $dsninfo;
    }

    public function getRef(Project $p)
    {
        if (!$this->checked) {
            $stk = array();
            array_push($stk, $this);
            $this->dieOnCircularReference($stk, $p);
        }
        $o = $this->ref->getReferencedObject($p);
        if (!($o instanceof MagentoEnv)) {
            throw new BuildException($this->ref->getRefId() . " doesn't denote a DSN");
        } else {
            return $o;
        }
    }
}
