<?php

use Phing\Task;

class ShowConfigTask extends Task
{

    /**
     * Whether to print env properties or no
     */
    protected $verbose = true;

    protected $path = 'app/etc/env.php';
    protected $areas = [];
    protected $config = [];
    /**
     * Set verbose
     *
     * @param string $str
     * @return void
     */
    public function setVerbose($str)
    {
        $this->verbose = StringHelper::booleanValue($str);
    }
    /**
     * Set path
     *
     * @param string $str
     * @return void
     */
    public function setPath($str)
    {
        $this->path = $str;
    }
    /**
     * Set area
     *
     * @param string $str
     * @return void
     */
    public function setAreas($str)
    {
        if (strlen(trim($str)) == 0) {
            return false;
        }
        $this->areas = explode(',', $str);
        foreach ($this->areas as $k => $area) {
            $this->area[$k] = trim($area);
        }
    }

    /**
     * The init method: Do init steps.
     */
    public function init()
    {
        $this->config = include $this->path;
    }

    /**
     * The main entry point method.
     */
    public function main()
    {
        if ($this->verbose) {
            $this->showConfig();
        }
    }

    private function showConfig()
    {
        if (count($this->areas) > 0) {
            foreach ($this->areas as $area) {
                $this->echoVector($this->array2path([$area => $this->config[$area]]));
            }
        } else {
            $this->echoVector($this->array2path($this->config));
        }
    }

    protected function array2path($treeArray, $parent = null)
    {
        $paths = [];
        if ($parent !== null) {
            $parent = $parent . '.';
        }
        foreach ($treeArray as $k => $v) {
            if (is_array($v)) {
                $paths = array_merge($paths, $this->array2path($v, $parent . $k));
            } else {
                $paths[] = (null !== $parent ? substr($parent, 0, -1) . '.' : '') . $k . '=' . $v;
            }
        }

        return $paths;
    }

    public function echoVector($arr)
    {
        foreach ($arr as $value) {
            echo $value . "\n";
        }
    }
}
