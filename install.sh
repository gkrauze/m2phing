#!/usr/bin/bash
cd ../
if ! command -v phing &> /dev/null
then
    wget https://www.phing.info/get/phing-latest.phar
    mv phing-latest.phar phing
    sudo chmod +x phing
fi
ln -s m2phing/build.xml build.xml
cp m2phing/buildcore.yaml buildcore.yaml
ln -s m2phing/buildcore.xml buildcore.xml
echo '/m2phing' >> .gitignore
echo '/build.xml' >> .gitignore
echo '/build.properties' >> .gitignore
echo '/buildcore.xml' >> .gitignore
echo '/buildcore.properties' >> .gitignore
echo '/magento.properties' >> .gitignore
echo -e "Build files are installed but before use: \n
1. setup buildcore.properties \n
2. setup database connection in app/etc/env.php" \n
