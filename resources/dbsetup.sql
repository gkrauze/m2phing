-- use your local url for the project
 
-- ACCORDING YOUR virtual host configuration
-- SET  @local_url PARAM PROPERLY!
-- set @local_url = CONCAT('http://localhost/', database(), '/');
-- set @local_url = CONCAT('http://', database(), '.local/');
set @local_url = 'http://apotera.local/';
 
-- set default admin password
set @admin_id = (SELECT user_id FROM admin_user WHERE username = 'store_@dmin');
set @admin_passwd_hash = 'bf5a89e1ce4a7f3f9bba3efc9e0d126540a3b88fdb955c57faa32660a0c563da:PJYV3xvlDk72fapiIZaIPADjFVnFvzoO:1'; -- Aadmin!1
DELETE FROM admin_passwords WHERE user_id = @admin_id;
UPDATE admin_user SET password=@admin_passwd_hash WHERE user_id = @admin_id;
INSERT INTO admin_passwords (user_id, password_hash, expires) VALUES (@admin_id, @admin_passwd_hash, 0);

-- base url
UPDATE core_config_data SET value = @local_url WHERE path = 'web/unsecure/base_url';
DELETE FROM core_config_data WHERE path = 'web/unsecure/base_link_url';
UPDATE core_config_data SET value = @local_url WHERE path = 'web/secure/base_url';
DELETE FROM core_config_data WHERE path = 'web/secure/base_link_url';
 
-- cookie
DELETE from core_config_data where path = 'web/cookie/cookie_domain';
 
-- search engine
UPDATE core_config_data SET value = 'mysql' WHERE path = 'catalog/search/engine';
 
-- full page cache
UPDATE core_config_data SET value = '1' WHERE path = 'system/full_page_cache/caching_application';
 
-- GA, GTM, Weltpixel GTM
update core_config_data set value = 0 where path in ('google/analytics/active', 'googletagmanager/general/active', 'weltpixel_googletagmanager/general/enable', 'google/googletagmanager/active', 'fj_universalanalytics/general/enable');
 
-- order copy
update core_config_data set value = null where path in (
'checkout/payment_failed/copy_to',
'sales_email/order/copy_to'
);
 
-- smtp (mail) for local env
update core_config_data set value = '0' where path = 'system/gmailsmtpapp/active';
update core_config_data set value = 'maildev.convert.no' where path = 'system/gmailsmtpapp/smtphost';
update core_config_data set value = '1025' where path = 'system/gmailsmtpapp/smtpport';
update core_config_data set value = 'NONE' where path = 'system/gmailsmtpapp/auth';
update core_config_data set value = 'none' where path = 'system/gmailsmtpapp/ssl';
update core_config_data set value = NULL where path = 'system/gmailsmtpapp/username';
update core_config_data set value = NULL where path = 'system/gmailsmtpapp/password';
 
-- disable magemonkey
update core_config_data set value = 0 where path = 'magemonkey/general/active';
UPDATE core_config_data SET VALUE = 0 WHERE path = 'mailchimp/general/active';
 
-- algolia
update core_config_data set value = 'fake' where path = 'algoliasearch_credentials/credentials/api_key';
UPDATE core_config_data SET value = 'magento2_dev_test_' WHERE path = 'algoliasearch_credentials/credentials/index_prefix';
 
-- nosto
DELETE from core_config_data where path = 'nosto_tagging/installation/id';
DELETE from core_config_data where path = 'nosto_tagging/settings/account';
DELETE from core_config_data where path = 'nosto_tagging/settings/tokens';
 
-- clerk
DELETE from core_config_data where path = 'clerk/general/public_key';
DELETE from core_config_data where path = 'clerk/general/private_key';
 
-- flow
update core_config_data set value = 0 where path = 'convert_flow/general/enabled';
 
-- dotmailer
update core_config_data set value = 0 where path = 'connector_api_credentials/api/enabled';
 
-- product alert
delete from core_config_data where path in ('catalog/productalert/allow_price', 'catalog/productalert/allow_stock');
 
-- promo magento_reminder (EE)
update core_config_data set value = 0 where path = 'promo/magento_reminder/enabled';
 
update core_config_data set value = 0 where path = 'dev/static/sign';
 
-- disable emails
UPDATE core_config_data SET VALUE = 0 WHERE path = 'system/gmailsmtpapp/active';
 
-- splitorders
UPDATE core_config_data SET value = 'local@mg.dev.convert.no' WHERE path = 'convert_split_orders/general/webshop_owner_email_identity';
 
-- Klarna
DELETE FROM core_config_data where path = 'klarna/api/merchant_id';
DELETE FROM core_config_data where path = 'klarna/api/shared_secret';
 
DELETE FROM core_config_data WHERE path = 'design/head/includes';
