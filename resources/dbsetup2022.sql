-- TODO: fix me

-- ACCORDING YOUR virtual host configuration
-- SET  @local_url PARAM PROPERLY!
-- set @local_url = CONCAT('http://localhost/', database(), '/');
-- set @local_url = CONCAT('http://', database(), '.local/');
set @local_url = 'https://bibsent.local/';

-- rename tables that were created by clenup script
-- RENAME TABLE `core_config_data_clear` TO `core_config_data`;

-- set default admin password
set @admin_id = (SELECT user_id FROM admin_user WHERE username = 'store_@dmin');
set @admin_passwd_hash = 'bf5a89e1ce4a7f3f9bba3efc9e0d126540a3b88fdb955c57faa32660a0c563da:PJYV3xvlDk72fapiIZaIPADjFVnFvzoO:1'; -- Aadmin!1
DELETE FROM admin_passwords WHERE user_id = @admin_id;
UPDATE admin_user SET password=@admin_passwd_hash WHERE user_id = @admin_id;
INSERT INTO admin_passwords (user_id, password_hash, expires) VALUES (@admin_id, @admin_passwd_hash, 0);

-- base url
UPDATE core_config_data SET value = @local_url WHERE path = 'web/unsecure/base_url';
DELETE FROM core_config_data WHERE path = 'web/unsecure/base_link_url';
UPDATE core_config_data SET value = @local_url WHERE path = 'web/secure/base_url';
DELETE FROM core_config_data WHERE path = 'web/secure/base_link_url';
