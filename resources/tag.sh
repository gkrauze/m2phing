#!/usr/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

BRANCH_NAME="feature/TRDNT-736-php8-compliance"
COMMIT_MESSAGE="[TRDNT-736] PHP8 compliance & code quality"
DIR_TO_CHECK="./vendor/convert/"

ONLY_LIST_CHANGES=false

# modules that are not maintained on master branch:
# convert/module-clerk (2.x)

# Parse named parameters
while [[ "$#" -gt 0 ]]; do
    case $1 in
        --only-list-changes) ONLY_LIST_CHANGES="$2"; shift ;;
        -l) ONLY_LIST_CHANGES="$2"; shift ;;
        -d) DIR_TO_CHECK="$2"; shift ;;
    esac
    shift
done

echo "Check directory: ${DIR_TO_CHECK}"
# cd ${DIR_TO_CHECK}
# echo "Current directory: $(pwd)"
dirs=($(ls -A ${DIR_TO_CHECK}))
echo "Found ${#dirs[@]} directories"
cd ${DIR_TO_CHECK}
for dir in "${dirs[@]}"; do

    cd "${dir}/"
    if [ ! -d .git ]; then 
        echo "Subdirectory ${dir} is not managed by git"
        cd ..
        continue
    fi
    changed_files_count=$(git ls-files -m | wc -l )
    if [ $changed_files_count -gt 0 ]; then
        echo -e "${YELLOW}"
        echo $PWD
        echo -e "${NC}"
        echo "  There is ${changed_files_count} files changed"
        PACKAGE_VERSION=$(jq '.version' <<< $(cat composer.json) | sed 's/[",]//g') 
        if [ $PACKAGE_VERSION == null ]; then 
            git fetch origin
            PACKAGE_VERSION=$(git describe origin/master --tags)
            echo -e "  Current version: ${YELLOW}${PACKAGE_VERSION}${NC}"
        else
            echo -e "  Current version: ${GREEN}${PACKAGE_VERSION}${NC}"
        fi
        NEXTVERSION=$(echo ${PACKAGE_VERSION} | awk -F. -v OFS=. '{$NF += 1 ; print}')
        echo -e "  Next version ${GREEN}${NEXTVERSION}${NC}"

        if [ $ONLY_LIST_CHANGES == true ]; then
            if [ -f CHANGELOG.md ]; then
                echo -e "  ${BLUE}${PWD}/CHANGELOG.md${NC}"
            else 
                echo "# Changelog" > CHANGELOG.md
                echo "" >> CHANGELOG.md
                echo "## [${NEXTVERSION}] - $(date +'%Y-%m-%d')" >> CHANGELOG.md
                echo "" >> CHANGELOG.md
                echo "### Changed" >> CHANGELOG.md
                git diff --name-only | while read file; do
                    echo "- ${file}" >> CHANGELOG.md
                done
            fi
            cd ..
            continue
        fi

        echo -e "${YELLOW}"
        git rev-parse --abbrev-ref HEAD
        echo -e "${NC}"
        read -p "  Do you want to proceed with git branch? (Y/n) " yn
        case $yn in
            [nN] ) cd ..; continue;;
            * ) ;;
        esac

        echo -e "${BLUE}git stash${NC}"
        git stash;
        echo -e "${BLUE}git co master${NC}"
        git co master;
        echo -e "${BLUE}git pull${NC}"
        git pull;
        echo -e "${BLUE}git create branch${NC}"
        git co -b ${BRANCH_NAME}
        echo -e "${BLUE}git stash pop${NC}"
        git stash pop
        contents=$(jq ".version=\"$NEXTVERSION\"" <<< $(cat composer.json)) && echo "${contents}" > composer.json
        echo -e "${RED}Changed composer.json${NC}"

        git fetch origin
        echo "Git last tag version on master: $(git describe origin/master --tags)"

        read -p "Do you want to proceed with git commit? (Y/n) " yn
        case $yn in
            [nN] ) cd ..; continue;;
            * ) ;;
        esac
        if grep -q "${NEXTVERSION}" CHANGELOG.md; then
            echo -e "  ${GREEN}CHANGELOG.md contains an entry for version ${NEXTVERSION}${NC}"
        else
            echo -e "  ${RED}CHANGELOG.md does not contain an entry for version ${NEXTVERSION}${NC}"
            read -p "  Do you want to skip the module? (Y) or push without CHANGELOG input (n) " yn
            case $yn in
                [yY] ) cd ..; continue;;
                * ) ;;
            esac
        fi
        echo -e "${BLUE}add${NC}";
        git add .;
        git commit -m "${COMMIT_MESSAGE}";

        echo -e "${BLUE}merge, commit and tag${NC}";
        git co master;
        git pull;
        git merge ${BRANCH_NAME}
        PACKAGE_VERSION=$(cat composer.json \
        | grep version \
        | head -1 \
        | awk -F: '{ print $2 }' \
        | sed 's/[",]//g')
        echo "  Create a tag: ${PACKAGE_VERSION}"
        git tag -a $PACKAGE_VERSION -m 'PHP8 compliance & code quality'
        git push && git push --tags
        echo -e "${GREEN}Done${NC}"
    fi
    cd ..
done